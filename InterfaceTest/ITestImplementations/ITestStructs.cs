﻿using InterfaceTest.Interfaces;

namespace InterfaceTest.InterfaceImplementations
{
    public struct StructWithoutDefaults : ITest
    {
        public int InterfaceProperty => 43;

        public string InterfaceMethod() =>
            $"{nameof(StructWithoutDefaults)}.{nameof(InterfaceMethod)}";


    }

    public struct StructWithDefaults : ITest
    {
        public int InterfaceProperty => 43 * 43;

        public int InterfaceDefaultProperty => ITest.InterfaceDefaultPropertyValue * 3;

        public string InterfaceMethod() =>
            $"{nameof(StructWithDefaults)}.{nameof(InterfaceMethod)}";


        public string InterfaceDefaultMethod() =>
            $"{nameof(StructWithDefaults)}.{nameof(InterfaceDefaultMethod)}";
    }
}
