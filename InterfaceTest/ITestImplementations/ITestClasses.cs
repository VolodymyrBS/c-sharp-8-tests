﻿using InterfaceTest.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceTest.InterfaceImplementations
{
    public class ClassWithoutDefaults : ITest
    {
        public int InterfaceProperty => 42;

        public string InterfaceMethod() =>
            $"{nameof(ClassWithoutDefaults)}.{nameof(InterfaceMethod)}";

        public void CallTestProtected()
        {
            // TestProtected();
            // ((ITest)this).TestProtected();
            ITest.CallProtectedStatic(this);
        }
    }

    public class ClassWithDefaults : ITest
    {
        public int InterfaceProperty => 42 * 42;

        public int InterfaceDefaultProperty => ITest.InterfaceDefaultPropertyValue * 2;

        public string InterfaceMethod() =>
            $"{nameof(ClassWithDefaults)}.{nameof(InterfaceMethod)}";

        public string InterfaceDefaultMethod() =>
            $"{nameof(ClassWithDefaults)}.{nameof(InterfaceDefaultMethod)}";
    }
}
