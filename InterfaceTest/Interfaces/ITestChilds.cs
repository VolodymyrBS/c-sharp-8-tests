﻿namespace InterfaceTest.Interfaces
{
    public interface ITestChildOverride : ITest
    {
        protected /*override */ new void TestProtected()
        {
            // base.TestProtected();
            // ((ITest)this).TestProtected();
            CallProtectedStatic(this);
        }
    }

    public interface ITestChildNew : ITest
    {
        protected void TestProtectedNew()
        {
            TestProtected();
            //((ITest)this).TestProtected();
            CallProtectedStatic(this);
        }
    }
}