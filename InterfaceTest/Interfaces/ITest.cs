﻿using System;

namespace InterfaceTest.Interfaces
{
    public interface ITest
    {
        int InterfaceProperty { get; }
        int InterfaceDefaultProperty => InterfaceDefaultPropertyValue;
        string InterfaceMethod();
        string InterfaceDefaultMethod() => "Default: " + InterfaceMethod();

        protected /*virtual*/ void TestProtected() => Console.WriteLine(nameof(TestProtected));

        protected static void CallProtectedStatic(ITest instance) => instance.TestProtected();

        static int InterfaceDefaultPropertyValueField;
        static readonly int InterfaceDefaultPropertyValue = 32;
    }
}
