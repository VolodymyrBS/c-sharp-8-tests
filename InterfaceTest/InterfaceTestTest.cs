﻿using BenchmarkDotNet.Attributes;
using InterfaceTest.InterfaceImplementations;
using InterfaceTest.Interfaces;

namespace InterfaceTest
{
    [CoreJob]
    [MemoryDiagnoser]
    public class InterfaceTestTest
    {
        ClassWithoutDefaults classWithoutDefaults = new ClassWithoutDefaults();
        ClassWithDefaults classWithDefaults = new ClassWithDefaults();
        StructWithoutDefaults structWithoutDefaults = new StructWithoutDefaults();
        StructWithDefaults structWithDefaults = new StructWithDefaults();

        public static int ITestUser(ITest itest)
        {
            var ret = itest.InterfaceProperty + itest.InterfaceDefaultProperty;
            itest.InterfaceMethod();
            itest.InterfaceDefaultMethod();
            return ret;
        }

        public static int ITestUserGeneric<T>(T itest) where T : ITest
        {
            var ret = itest.InterfaceProperty + itest.InterfaceDefaultProperty;
            itest.InterfaceMethod();
            itest.InterfaceDefaultMethod();
            return ret;
        }

        [Benchmark]
        public int TestInterfaceClassWithoutDefaults() => ITestUser(classWithoutDefaults);
        [Benchmark]
        public int TestInterfaceClassWithDefaults() => ITestUser(classWithDefaults);
        [Benchmark]
        public int TestInterfaceStructWithoutDefaults() => ITestUser(structWithoutDefaults);
        [Benchmark]
        public int TestInterfaceStructWithDefaults() => ITestUser(structWithDefaults);
        [Benchmark]
        public int TestGenericClassWithoutDefaults() => ITestUserGeneric(classWithoutDefaults);
        [Benchmark]
        public int TestGenericClassWithDefaults() => ITestUserGeneric(classWithDefaults);
        [Benchmark]
        public int TestGenericStructWithoutDefaults() => ITestUserGeneric(structWithoutDefaults);
        [Benchmark]
        public int TestGenericStructWithDefaults() => ITestUserGeneric(structWithDefaults);

        /*
            |                             Method |       Mean |     Error |    StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
            |----------------------------------- |-----------:|----------:|----------:|-------:|------:|------:|----------:|
            |  TestInterfaceClassWithoutDefaults | 30.4355 ns | 0.5882 ns | 0.5502 ns | 0.0268 |     - |     - |     112 B |
            |     TestInterfaceClassWithDefaults |  7.8150 ns | 0.0780 ns | 0.0729 ns |      - |     - |     - |         - |
            | TestInterfaceStructWithoutDefaults | 39.7190 ns | 0.8250 ns | 1.2349 ns | 0.0344 |     - |     - |     144 B |
            |    TestInterfaceStructWithDefaults | 17.6635 ns | 0.3571 ns | 0.3507 ns | 0.0057 |     - |     - |      24 B |
            |    TestGenericClassWithoutDefaults | 35.4563 ns | 0.5639 ns | 0.5275 ns | 0.0268 |     - |     - |     112 B |
            |       TestGenericClassWithDefaults |  7.9297 ns | 0.0985 ns | 0.0922 ns |      - |     - |     - |         - |
            |   TestGenericStructWithoutDefaults | 39.4460 ns | 0.8805 ns | 1.6320 ns | 0.0344 |     - |     - |     144 B |
            |      TestGenericStructWithDefaults |  0.8047 ns | 0.0089 ns | 0.0074 ns |      - |     - |     - |         - |
        */

    }
}
