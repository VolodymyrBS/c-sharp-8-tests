﻿using BenchmarkDotNet.Running;
using InterfaceTest.Interfaces;
using InterfaceTest.InterfaceImplementations;
using System;

#nullable enable
namespace InterfaceTest
{
    class InterfaceTestMain
    {
        public static void ITestUser(ITest itest)
        {
            Console.WriteLine($"---{nameof(ITestUser)}: {itest.GetType()}---");
            Console.WriteLine(itest.InterfaceProperty); // 30 - 
            Console.WriteLine(itest.InterfaceDefaultProperty); // 38
            itest.InterfaceMethod(); // 40
            itest.InterfaceDefaultMethod(); // 48
            Console.WriteLine();
        }

        public static void ITestUserGeneric<T>(T itest) where T : ITest
        {
            Console.WriteLine($"---{nameof(ITestUser)}: {typeof(T)}---");
            Console.WriteLine(itest.InterfaceProperty);
            Console.WriteLine(itest.InterfaceDefaultProperty); // 00007ffc7bd430e0 
            itest.InterfaceMethod();
            itest.InterfaceDefaultMethod();
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            var classWithoutDefaults = new ClassWithoutDefaults();
            var classWithDefaults = new ClassWithDefaults();
            var structWithoutDefaults = new StructWithoutDefaults();
            var structWithDefaults = new StructWithDefaults();

            ITestUser(classWithoutDefaults);
            ITestUser(classWithDefaults);
            ITestUserGeneric(classWithoutDefaults);
            ITestUserGeneric(classWithDefaults);
            ITestUser(structWithoutDefaults);
            ITestUser(structWithDefaults);
            ITestUserGeneric(structWithoutDefaults);
            ITestUserGeneric(structWithDefaults);

            classWithoutDefaults.CallTestProtected();

            BenchmarkRunner.Run<InterfaceTestTest>();
        }
    }
}
#nullable disable
